# nacos docker
> 官方镜像: https://github.com/nacos-group/nacos-docker

## standalone
> 由于官方镜像过大，故简化相关配置，Dockerfile 基于 openjdk:8-jdk-alpine 构建

构建命令比较简陋
- `cd standalone`
- `docker-compose build`
- `docker-compose up -d`
- `docker-compose logs -f`

注意事项
- 如果启动报错 `bin/docker-startup.sh: set: line 14: illegal option -`
  > 可能是 `standalone/bin/docker-startup.sh` 文件编码异常，`doc/backup/docker-startup.zip` 解压替换下

浏览器访问：http://127.0.0.1:8848/nacos/

![nacos-build-standalone](doc/image/nacos-build-standalone.png)

## raspberry
> 简便起见，先把 Dockerfile 改好，构建时直接复制一份 standalone（bin、conf、init.d、target）即可

* 构建镜像 `docker buildx build --platform linux/arm/v7 -t amos0626/nacos .`
* push镜像 `docker push amos0626/nacos`

## 属性配置列表

| 属性名称                          | 描述                                                         | 选项                              |
| --------------------------------- | ------------------------------------------------------------ | ----------------------------------- |
| MODE                              | 系统启动方式: 集群/单机                                      | cluster/standalone默认 **cluster**  |
| NACOS_SERVERS                     | nacos cluster address                                        | p1:port1空格ip2:port2 空格ip3:port3 |
| PREFER_HOST_MODE                  | 支持IP还是域名模式                                           | hostname/ip 默认 **ip**             |
| NACOS_SERVER_PORT                 | Nacos 运行端口                                               | 默认 **8848**                       |
| NACOS_SERVER_IP                   | 多网卡模式下可以指定IP                                       |                                     |
| SPRING_DATASOURCE_PLATFORM        | standalone support mysql                                     | mysql / 空 默认:空                 |
| MYSQL_SERVICE_HOST                | mysql  host                                                  |                                     |
| MYSQL_SERVICE_PORT                | mysql  database port                                         | 默认 : **3306**                  |
| MYSQL_SERVICE_DB_NAME             | mysql  database name                                         |                                     |
| MYSQL_SERVICE_USER                | username of  database                                        |                                     |
| MYSQL_SERVICE_PASSWORD            | password of  database                                        |                                     |
| MYSQL_DATABASE_NUM                | It indicates the number of database                          | 默认 :**1**                    |
| JVM_XMS                           | -Xms                                                         | 默认 :2g                       |
| JVM_XMX                           | -Xmx                                                         | 默认 :2g                       |
| JVM_XMN                           | -Xmn                                                         | 默认 :1g                       |
| JVM_MS                            | -XX:MetaspaceSize                                            | 默认 :128m                     |
| JVM_MMS                           | -XX:MaxMetaspaceSize                                         | 默认 :320m                     |
| NACOS_DEBUG                       | enable remote debug                                          | y/n 默认 :n                      |
| TOMCAT_ACCESSLOG_ENABLED          | server.tomcat.accesslog.enabled                              | 默认 :false                      |
| NACOS_AUTH_SYSTEM_TYPE      |  权限系统类型选择,目前只支持nacos类型       | 默认 :nacos                          |
| NACOS_AUTH_ENABLE      |  是否开启权限系统       | 默认 :false                          |
| NACOS_AUTH_TOKEN_EXPIRE_SECONDS      |  token 失效时间        | 默认 :18000                          |
| NACOS_AUTH_TOKEN      |  token       | 默认 :SecretKey012345678901234567890123456789012345678901234567890123456789                          |
| NACOS_AUTH_CACHE_ENABLE      |  权限缓存开关 ,开启后权限缓存的更新默认有15秒的延迟      | 默认 : false                          |


## Nacos + Grafana + Prometheus

使用参考：[Nacos monitor-guide](https://nacos.io/zh-cn/docs/monitor-guide.html)

**Note**:  当使用Grafana创建数据源的时候地址必须是: **http://prometheus:9090**